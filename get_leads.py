import argparse
import json
import requests
import csv
import traceback
import sys
from datetime import datetime
from generate_token import get_token


GET_LEADS_URL = "https://yogamiprivatelimited.amocrm.com/api/v4/leads"
SOFT_LIMIT = 20
PAGE_LIMIT = 100
DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"


def unix_time_millis(dt: datetime):
    epoch = datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds()


def get_auth_headers() -> dict:
    return {
        "Authorization": get_token()
    }


def get_timestamp(epoch: str) -> str:
    if epoch is None:
        return ''
    return datetime.fromtimestamp(float(epoch)).strftime(DATE_TIME_FORMAT)


def get_params(page, start=None, end=None) -> dict:
    params = {
        "page": page,
        "with": "loss_reason,contacts",
        "limit": 250
    }
    if start is not None:
        params["filter[created_at][from]"] = unix_time_millis(start)
    if end is not None:
        params["filter[created_at][to]"] = unix_time_millis(end)
    return params


def get_leads(start=None, end=None) -> list:
    leads_data = []
    url = GET_LEADS_URL
    call_count = 1
    headers = get_auth_headers()

    try:
        while True:
            response = requests.request("GET", url, headers=headers, data={}, params=get_params(call_count, start, end))

            if response.status_code == 204:
                break

            response_data = json.loads(response.content)

            print(f"Getting response: {response_data}")

            if response_data["_links"].get("next") is None:
                break

            if SOFT_LIMIT != -1 and call_count == SOFT_LIMIT:
                break

            for leads in response_data["_embedded"]["leads"]:
                leads_record = {}

                # adding custom fields
                if leads.get("custom_fields_values") is not None:
                    for field in leads["custom_fields_values"]:
                        leads[field["field_name"]] = field["values"]

                # adding the expected fields
                leads_record["id"] = leads.get("id")
                leads_record["status_id"] = leads.get("status_id")
                leads_record["responsible_user_id"] = leads.get("responsible_user_id")
                leads_record["name"] = leads.get("name")
                leads_record["pipeline_id"] = leads.get("pipeline_id")
                leads_record["group_id"] = leads.get("group_id")
                leads_record["price"] = leads.get("price")
                leads_record["account_id"] = leads.get("account_id")

                # adding tags
                if leads["_embedded"].get("tags") is not None:
                    leads_record["tags"] = ','.join([x["name"] for x in leads["_embedded"]["tags"]])

                # adding contact_id
                if leads["_embedded"].get("contacts") is not None and len(leads["_embedded"]["contacts"]) > 0:
                    leads_record["contacts"] = leads["_embedded"]["contacts"][0]["id"]

                # updating the time fields
                leads_record["updated_at"] = get_timestamp(leads["updated_at"])
                leads_record["closed_at"] = get_timestamp(leads["closed_at"])
                leads_record["closest_task_at"] = get_timestamp(leads["closest_task_at"])
                leads_record["created_at"] = get_timestamp(leads["created_at"])

                # adding loss reason
                if leads["_embedded"].get("loss_reason") is not None and len(leads["_embedded"]["loss_reason"]) > 0:
                    leads_record["loss_reason"] = leads["_embedded"]["loss_reason"][0]["name"]


                # adding custom fields - trainer, lead name, day, reminder, channel, client_summary
                if leads.get("Trainer (PT)") is not None:
                    leads_record["trainer"] = leads["Trainer (PT)"][0]["value"]
                if leads.get("LEAD NAME") is not None:
                    leads_record["lead_name"] = leads["LEAD NAME"][0]["value"]
                if leads.get("DAY") is not None:
                    leads_record["day"] = leads["DAY"][0]["value"]
                if leads.get("Reminder") is not None:
                    leads_record["reminder"] = leads["Reminder"][0]["value"]
                if leads.get("Channel") is not None:
                    leads_record["channel"] = leads["Channel"][0]["value"]
                if leads.get("SUMMARY OF THE CLIENT") is not None:
                    leads_record["client_summary"] = leads["SUMMARY OF THE CLIENT"][0]["value"]

                # updating the custom time fields - trial_date, preferred_time, inbound_date,
                # conversion_date, subscription_expiry
                if leads.get("Trial Date") is not None:
                    leads_record["trial_date"] = get_timestamp(leads.get("Trial Date")[0]["value"])
                if leads.get("Preferred time") is not None:
                    leads_record["preferred_time"] = leads["Preferred time"][0]["value"]
                if leads.get("Inbound Date") is not None:
                    leads_record["inbound_date"] = get_timestamp(leads["Inbound Date"][0]["value"])
                if leads.get("Conversion date") is not None:
                    leads_record["conversion_date"] = get_timestamp(leads["Conversion date"][0]["value"])
                if leads.get("SUBSCRIPTION EXPIRY") is not None:
                    leads_record["subscription_expiry"] = get_timestamp(leads["SUBSCRIPTION EXPIRY"][0]["value"])

                leads_data.append(leads_record)

            call_count += 1
    except Exception as e:
        print("Unable to fetch events due to exception:", e)
        # printing stack trace
        traceback.print_exception(*sys.exc_info())
    return leads_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Fetching leads data...")
    parser.add_argument("-s", "--start", help="Start date for the query. Ex: 2020-01-01", required=False, default=None)
    parser.add_argument("-e", "--end", help="Start date for the query. Ex: 2022-01-01", required=False, default=None)

    argument = parser.parse_args()

    start_date = None
    end_date = None

    try:
        if argument.start is not None:
            start_date = datetime.fromisoformat(argument.start)
        if argument.end is not None:
            end_date = datetime.fromisoformat(argument.end)
    except Exception as e:
        print("Unable to validate inputs: ", e)
    else:
        leads_table = get_leads(start_date, end_date)

        headers = ["id"]

        for lead in leads_table:
            for column_header in lead.keys():
                if column_header not in headers:
                    headers.append(column_header)

        with open("../data/leads.csv", "w", newline="", encoding='utf-8') as f:
            dict_writer = csv.DictWriter(f, headers)
            dict_writer.writeheader()
            dict_writer.writerows(leads_table)