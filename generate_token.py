import json

import requests

TOKEN_REFRESH_URL = "https://yogamiprivatelimited.amocrm.com/oauth2/access_token"


def get_refresh_token():
    with open("token.txt", "r") as f:
        return f.read().strip()


def set_refresh_token(refresh_token):
    with open("token.txt", "w") as f:
        return f.write(refresh_token)


def get_token() -> str:
    refresh_token = get_refresh_token()

    payload = json.dumps({
        "client_id": "a27cba8b-48d7-48cb-aaf0-3f4d75ae1e8f",
        "client_secret": "VAqB6lXSL9dimwpghKWtRyj2Q3XSfPfrGmqEtTIxe8nMLVETKzmmZ401V1pM1S91",
        "grant_type": "refresh_token",
        "refresh_token": refresh_token,
        "redirect_uri": "https://www.yogami.fit"
    })
    headers = {
        'Content-Type': 'application/json',
    }

    response = requests.request("POST", TOKEN_REFRESH_URL, headers=headers, data=payload)
    response_data = json.loads(response.content)

    print("Getting response: {}", response_data)

    access_token = response_data.get("access_token")
    refresh_token = response_data.get("refresh_token")

    set_refresh_token(refresh_token)
    print(f"Generating token: {access_token}")
    return f"Bearer {access_token}"


if __name__ == "__main__":
    print("Token: {}", get_token())