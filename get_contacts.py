import argparse
import json
import requests
import csv
import sys
import traceback
from datetime import datetime
from generate_token import get_token

GET_CONTACTS_URL = "https://yogamiprivatelimited.amocrm.com/api/v4/contacts"
SOFT_LIMIT = 20
PAGE_LIMIT = 100
DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"


def unix_time_millis(dt: datetime):
    epoch = datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds()


def get_auth_headers() -> dict:
    return {
        "Authorization": get_token()
    }


def get_timestamp(epoch: str) -> str:
    if epoch is None:
        return ''
    return datetime.fromtimestamp(float(epoch)).strftime(DATE_TIME_FORMAT)


def get_params(page, start=None, end=None) -> dict:
    params = {
        "page": page,
        "limit": 250
    }
    if start is not None:
        params["filter[created_at][from]"] = unix_time_millis(start)
    if end is not None:
        params["filter[created_at][to]"] = unix_time_millis(end)
    return params


def get_contacts(start=None, end=None) -> list:
    contacts_data = []
    url = GET_CONTACTS_URL
    call_count = 1
    headers = get_auth_headers()

    try:
        while True:
            response = requests.request("GET", url, headers=headers, data={}, params=get_params(call_count, start, end))
            
            if response.status_code == 204:
                break

            response_data = json.loads(response.content)
            
            print(f"Getting response: {response_data}")

            if response_data["_links"].get("next") is None:
                break

            if SOFT_LIMIT != -1 and call_count == SOFT_LIMIT:
                break

            for contacts in response_data["_embedded"]["contacts"]:

                contacts_record = {}

                # adding custom fields
                if contacts.get("custom_fields_values") is not None:
                    for field in contacts["custom_fields_values"]:
                        contacts[field["field_name"]] = field["values"]

                # adding the expected fields - id, responsible_user_id, name, is_deleted, account_id, first_name, last_name
                contacts_record["id"] = contacts.get("id")
                contacts_record["name"] = contacts.get("name")
                contacts_record["responsible_user_id"] = contacts.get("responsible_user_id")
                contacts_record["is_deleted"] = contacts.get("is_deleted")
                contacts_record["account_id"] = contacts.get("account_id")
                contacts_record["first_name"] = contacts.get("first_name")
                contacts_record["last_name"] = contacts.get("last_name")
                contacts_record["created_by"] = contacts.get("created_by")
                contacts_record["updated_by"] = contacts.get("updated_by")

                # adding the timestamp fields - created_at, updated_at, created_by, updated_by, closest_task_at
                contacts_record["created_at"] = get_timestamp(contacts.get("created_at"))
                contacts_record["updated_at"] = get_timestamp(contacts.get("updated_at"))
                contacts_record["closest_task_at"] = get_timestamp(contacts.get("closest_task_at"))

                # adding the email details
                if contacts.get("Email") is not None:
                    for email in contacts.get("Email"):
                        if email.get("enum_code") == "WORK":
                            contacts_record["work_email"] = email.get("value")
                        if email.get("enum_code") == "OTHER":
                            contacts_record["work_email"]  = email.get("value")

                # adding the phone number details
                if contacts.get("Phone") is not None:
                    for phone in contacts.get("Phone"):
                        if phone.get("enum_code") == "WORK":
                            contacts_record["work_phone"] = phone.get("value")
                        if phone.get("enum_code") == "MOB":
                            contacts_record["work_phone"] = phone.get("value")

                contacts_data.append(contacts_record)

            call_count += 1
    except Exception as e:
        print("Unable to fetch contacts due to exception:", e)
        # printing stack trace
        traceback.print_exception(*sys.exc_info())
    return contacts_data


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Fetching contacts data...")
    parser.add_argument("-s", "--start", help="Start date for the query. Ex: 2020-01-01", required=False, default=None)
    parser.add_argument("-e", "--end", help="Start date for the query. Ex: 2022-01-01", required=False, default=None)

    argument = parser.parse_args()

    start_date = None
    end_date = None

    try:
        if argument.start is not None:
            start_date = datetime.fromisoformat(argument.start)
        if argument.end is not None:
            end_date = datetime.fromisoformat(argument.end)
    except Exception as e:
        print("Unable to validate inputs: ", e)
    else:
        contacts_table = get_contacts(start_date, end_date)

        headers = ["id", "name"]
        for contact in contacts_table:
            for column_header in contact.keys():
                if column_header not in headers:
                    headers.append(column_header)

        with open("../data/contacts.csv", "w", newline="", encoding='utf-8') as f:
            dict_writer = csv.DictWriter(f, headers)
            dict_writer.writeheader()
            dict_writer.writerows(contacts_table)