import json
import requests
import csv
from generate_token import get_token

GET_PIPELINES_URL = "https://yogamiprivatelimited.amocrm.com/api/v4/leads/pipelines"
IGNORE_LIST = ["account_id", "sort", "_links"]


def get_auth_headers() -> dict:
    return {
        "Authorization": get_token()
    }

def get_pipelines() -> list:
    pipelines_data = []
    try:
        response = requests.request("GET", GET_PIPELINES_URL, headers=get_auth_headers(), data={}, params={})
        response_data = json.loads(response.content)

        print(f"Getting response: {response_data}")

        for pipeline in response_data["_embedded"]["pipelines"]:
            for status in pipeline["_embedded"]["statuses"]:
                status["pipeline_name"] = pipeline["name"]
                [status.pop(key) for key in IGNORE_LIST]
                pipelines_data.append(status)

    except Exception as e:
        print("Unable to fetch events due to exception:", e)
    return pipelines_data


if __name__ == '__main__':
    print("Fetching pipelines data...")
    pipelines = get_pipelines()
    headers = pipelines[0].keys()

    with open("../data/pipeline.csv", "w", newline="", encoding='utf-8') as f:
        dict_writer = csv.DictWriter(f, headers)
        dict_writer.writeheader()
        dict_writer.writerows(pipelines)