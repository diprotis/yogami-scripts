# README #
Fetch data from amoCRM

*Commands:
1. Get all contacts created between start date (-s) and end date (-e)
> python get_contacts.py -s "2022-05-31" -e "2022-06-21"

2. Get all leads created between start date (-s) and end date (-e)
> python get_leads.py -s "2022-05-31" -e "2022-06-21"

3. Get all events where lead status was changed (-t "lead_status_changed") between start date (-s) and end date (-e)
> python get_events.py -t "lead_status_changed" -s "2022-06-21" -e "2022-07-04"

3. Get all pipelines and status
> python get_pipelines.py

python get_all.py -t "lead_status_changed" -s "2022-07-11" -e "2022-07-25"