import argparse
import json
import sys
import traceback
from datetime import datetime
import requests
import csv
from generate_token import get_token

GET_EVENTS_URL = "https://yogamiprivatelimited.amocrm.com/api/v4/events?page=1&limit=100"
SOFT_LIMIT = 20
IGNORE_LIST = ["account_id", "_embedded", "_links"]
DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"


def unix_time_millis(dt: datetime):
    epoch = datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds()


def get_auth_headers() -> dict:
    return {
        "Authorization": get_token()
    }


def get_params(entity_type=None, event_type=None, start=None, end=None) -> dict:
    params = {}
    if entity_type is not None:
        params["filter[entity][]"] = entity_type
    if event_type is not None:
        params["filter[type][]"] = event_type
    if start is not None:
        params["filter[created_at][from]"] = unix_time_millis(start)
    if end is not None:
        params["filter[created_at][to]"] = unix_time_millis(end)
    return params


def get_timestamp(epoch: str) -> str:
    if epoch is None:
        return ''
    return datetime.fromtimestamp(float(epoch)).strftime(DATE_TIME_FORMAT)


def get_events(start=None, end=None, entity_type=None, event_type=None) -> list:
    print(f"Fetching all the events for start_date: {start} end_date: {end} "
          f"and entity_filter: {entity_type} and event_type: {event_type}")

    events_data = []
    url = GET_EVENTS_URL
    call_count = 0
    headers = get_auth_headers()
    params = get_params(entity_type, event_type, start, end)

    try:
        while True:
            response = requests.request("GET", url, headers=headers, data={}, params=params)
            if response.status_code == 204:
                break
            response_data = json.loads(response.content)

            print(f"Getting response: {response_data}")

            if response_data["_links"].get("next") is None:
                break

            if SOFT_LIMIT != -1 and call_count == SOFT_LIMIT:
                break

            url = response_data["_links"]["next"]["href"]
            print(f"Page: {url}")
            for event in response_data["_embedded"]["events"]:

                # updating the timestamp columns
                event["created_at"] = get_timestamp(event["created_at"])

                # adding to the final list
                events_data.append(event)

            call_count += 1
    except Exception as e:
        print("Unable to fetch events due to exception:", e)
        # printing stack trace
        traceback.print_exception(*sys.exc_info())
    return events_data


def ignore_keys(event: dict) -> dict:
    [event.pop(key) for key in IGNORE_LIST]
    return event


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Fetching events data...")
    parser.add_argument("-s", "--start", help="Start date for the query. Ex: 2020-01-01", required=False, default=None)
    parser.add_argument("-e", "--end", help="Start date for the query. Ex: 2022-01-01", required=False, default=None)
    parser.add_argument("-y", "--entity", help="Entity type. Ex: lead|contact", required=False, default=None)
    parser.add_argument("-t", "--type", help="Event Type. Ex: entity_linked,entity_tag_added", required=False,
                        default=None)

    argument = parser.parse_args()

    start_date = None
    end_date = None
    entity = None
    type = None

    try:
        if argument.start is not None:
            start_date = datetime.fromisoformat(argument.start)
        if argument.end is not None:
            end_date = datetime.fromisoformat(argument.end)
        entity = argument.entity
        type = argument.type
    except Exception as e:
        print("Unable to validate inputs: ", e)
    else:
        events = list(map(ignore_keys, get_events(start=start_date, end=end_date, entity_type=entity, event_type=type)))
        headers = events[0].keys()

        with open("../data/events.csv", "w", newline="", encoding='utf-8') as f:
            dict_writer = csv.DictWriter(f, headers)
            dict_writer.writeheader()
            dict_writer.writerows(events)