import argparse
import os

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Fetching contacts data...")
    parser.add_argument("-s", "--start", help="Start date for the query. Ex: 2020-01-01", required=False, default=None)
    parser.add_argument("-e", "--end", help="Start date for the query. Ex: 2022-01-01", required=False, default=None)
    parser.add_argument("-y", "--entity", help="Entity type. Ex: lead|contact", required=False, default=None)
    parser.add_argument("-t", "--type", help="Event Type. Ex: entity_linked,entity_tag_added", required=False, default=None)

    argument = parser.parse_args()

    try:
        os.system("get_pipelines.py")
        events_command = "get_events.py "
        leads_command = "get_leads.py "
        contacts_command = "get_contacts.py "

        if argument.start is not None:
            events_command = events_command + "-s " + argument.start
            leads_command = leads_command + "-s " + argument.start
            contacts_command = contacts_command + "-s " + argument.start
        if argument.end is not None:
            events_command = events_command + " -e " + argument.end
            leads_command = leads_command + " -e " + argument.end
            contacts_command = contacts_command + " -e " + argument.end
        if argument.type is not None:
            events_command = events_command + " -t " + argument.type
        if argument.entity is not None:
            events_command = events_command + " -t " + argument.entity

        os.system("get_pipelines.py")
        os.system(leads_command)
        os.system(contacts_command)
        os.system(events_command)

    except Exception as e:
        print("Unable to run scripts: ", e)
